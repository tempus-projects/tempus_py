pytempus
========

Python bindings for the [Tempus](http://ifsttar.github.io/Tempus/) framework.

See the [documentation on readthedocs](http://pytempus.readthedocs.io).