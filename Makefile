#x86_64-linux-gnu-gcc -pthread -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -g -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -I/usr/include/python3.5m -c src/pytempus.cc -o build/temp.linux-x86_64-3.5/src/pytempus.o

#x86_64-linux-gnu-g++ -pthread -shared -Wl,-O1 -Wl,-Bsymbolic-functions -Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-Bsymbolic-functions -Wl,-z,relro -g -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 build/temp.linux-x86_64-3.5/src/pytempus.o build/temp.linux-x86_64-3.5/src/cost.o build/temp.linux-x86_64-3.5/src/multimodal_graph.o build/temp.linux-x86_64-3.5/src/plugin.o build/temp.linux-x86_64-3.5/src/plugin_factory.o build/temp.linux-x86_64-3.5/src/poi.o build/temp.linux-x86_64-3.5/src/point.o build/temp.linux-x86_64-3.5/src/progression.o build/temp.linux-x86_64-3.5/src/public_transport.o build/temp.linux-x86_64-3.5/src/request.o build/temp.linux-x86_64-3.5/src/road_graph.o build/temp.linux-x86_64-3.5/src/roadmap.o build/temp.linux-x86_64-3.5/src/routing_data.o build/temp.linux-x86_64-3.5/src/transport_mode.o build/temp.linux-x86_64-3.5/src/variant.o -ltempus -lboost_python-py35 -o build/lib.linux-x86_64-3.5/pytempus.cpython-35m-x86_64-linux-gnu.so

SOURCES = src/cost.cc src/plugin_factory.cc src/progression.cc src/request.cc src/routing_data.cc \
	src/multimodal_graph.cc src/poi.cc src/public_transport.cc src/road_graph.cc src/transport_mode.cc \
	src/plugin.cc src/point.cc src/pytempus.cc src/roadmap.cc src/variant.cc

COMPILE_FLAGS = -pthread -DNDEBUG -g -O2 -Wall -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2 -fPIC -std=c++11

LD_FLAGS = -pthread -shared -Wl,-O1 -Wl,-Bsymbolic-functions -Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-Bsymbolic-functions -Wl,-z,relro -g -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2

PYVER = $(shell python3 -c "import sys;print('%d%d'%sys.version_info[0:2])")
LIBS = -ltempus -lboost_python-py$(PYVER)

ARCH_SUFFIX= $(shell python3 -c "import sys; import platform; print('%s-%s-%d.%d'%((sys.platform,platform.machine())+sys.version_info[0:2]))")

TEMPDIR = build/temp.$(ARCH_SUFFIX)
OUTDIR = build/lib.$(ARCH_SUFFIX)

OBJECTS = $(patsubst %.cc, $(TEMPDIR)/%.o, $(SOURCES))

INCLUDEPY = $(shell python3 -c "import sysconfig; print(sysconfig.get_config_var('INCLUDEPY'))")
EXT_SUFFIX = $(shell python3 -c "import sysconfig; print(sysconfig.get_config_var('EXT_SUFFIX'))")

OUT = $(OUTDIR)/pytempus$(EXT_SUFFIX)

$(TEMPDIR)/%.o : %.cc
	mkdir -p $(TEMPDIR)/src
	g++ $(COMPILE_FLAGS) -I$(INCLUDEPY) -c $< -o $@

$(OUT): $(OBJECTS)
	mkdir -p $(OUTDIR)
	g++ $(LD_FLAGS) $(OBJECTS) $(LIBS) -o $(OUT)

