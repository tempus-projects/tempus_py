Utilities
=========

This documentation page gather all functions contained into `samples/utils.py` module. If not working, please consider module docstrings.

.. automodule:: samples.utils

.. autofunction:: samples.utils.road_node_id_from_coordinates

.. autofunction:: samples.utils.sample_node

.. autofunction:: samples.utils.get_transport_modes

.. autofunction:: samples.utils.browse
